﻿namespace HistoryQuiz
{
    partial class Result
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Result));
            this.himikoText = new System.Windows.Forms.Label();
            this.resultText = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.himikoPicture = new System.Windows.Forms.PictureBox();
            this.effectPicture = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.himikoPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.effectPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // himikoText
            // 
            this.himikoText.AutoSize = true;
            this.himikoText.Font = new System.Drawing.Font("游明朝 Demibold", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.himikoText.Location = new System.Drawing.Point(110, 150);
            this.himikoText.Name = "himikoText";
            this.himikoText.Size = new System.Drawing.Size(93, 37);
            this.himikoText.TabIndex = 2;
            this.himikoText.Text = "label1";
            // 
            // resultText
            // 
            this.resultText.AutoSize = true;
            this.resultText.Font = new System.Drawing.Font("游明朝 Demibold", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.resultText.Location = new System.Drawing.Point(163, 32);
            this.resultText.Name = "resultText";
            this.resultText.Size = new System.Drawing.Size(110, 45);
            this.resultText.TabIndex = 3;
            this.resultText.Text = "label2";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(16, 105);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(489, 248);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // himikoPicture
            // 
            this.himikoPicture.Location = new System.Drawing.Point(524, 205);
            this.himikoPicture.Name = "himikoPicture";
            this.himikoPicture.Size = new System.Drawing.Size(277, 270);
            this.himikoPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.himikoPicture.TabIndex = 0;
            this.himikoPicture.TabStop = false;
            // 
            // effectPicture
            // 
            this.effectPicture.Location = new System.Drawing.Point(511, 79);
            this.effectPicture.Name = "effectPicture";
            this.effectPicture.Size = new System.Drawing.Size(153, 120);
            this.effectPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.effectPicture.TabIndex = 4;
            this.effectPicture.TabStop = false;
            // 
            // Result
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.effectPicture);
            this.Controls.Add(this.resultText);
            this.Controls.Add(this.himikoText);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.himikoPicture);
            this.Name = "Result";
            this.Size = new System.Drawing.Size(801, 475);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.himikoPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.effectPicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox himikoPicture;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label himikoText;
        private System.Windows.Forms.Label resultText;
        private System.Windows.Forms.PictureBox effectPicture;
    }
}
