﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HistoryQuiz
{
    public partial class Form1 : Form
    {
        public static int score = 0;


        public static Start start;
        public static Q1 q1;
        public static Q2 q2;
        public static Q3 q3;
        public static Q4 q4;
        public static Q5 q5;
        public static Q6 q6;
        public static Q7 q7;
        public static Q8 q8;
        public static Q9 q9;
        public static Q10 q10;
        public static Result result;
     


        public Form1()
        {
            InitializeComponent();

            this.StartPosition = FormStartPosition.CenterScreen;
            start = new Start();
            q1 = new Q1();
            q2 = new Q2();
            q3 = new Q3();
            q4 = new Q4();
            q5 = new Q5();
            q6 = new Q6();
            q7 = new Q7();
            q8 = new Q8();
            q9 = new Q9();
            q10 = new Q10();
            result = new Result();

            mainPanel.Controls.Add(start);
            mainPanel.Controls.Add(q1);
            mainPanel.Controls.Add(q2);
            mainPanel.Controls.Add(q3);
            mainPanel.Controls.Add(q4);
            mainPanel.Controls.Add(q5);
            mainPanel.Controls.Add(q6);
            mainPanel.Controls.Add(q7);
            mainPanel.Controls.Add(q8);
            mainPanel.Controls.Add(q9);
            mainPanel.Controls.Add(q10);
            mainPanel.Controls.Add(result);

            start.Visible = true;
            q1.Visible = false;
            q2.Visible = false;
            q3.Visible = false;
            q4.Visible = false;
            q5.Visible = false;
            q6.Visible = false;
            q7.Visible = false;
            q8.Visible = false;
            q9.Visible = false;
            q10.Visible = false;
            result.Visible = false;
        }


    }
}
