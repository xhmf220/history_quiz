﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HistoryQuiz
{
    public partial class Start : UserControl
    {
        public Start()
        {
            InitializeComponent();
            label1.BackColor = Color.Transparent;
        }

        private void StartButtonClicked(object sender, EventArgs e)
        {
            Form1.start.Visible = false;
            Form1.q1.Visible = true;
            Form1.q2.Visible = false;
        }
    }
}
