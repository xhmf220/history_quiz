﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HistoryQuiz
{
    public partial class Q1 : UserControl
    {

        
        
        public Q1()
        {
            InitializeComponent();
            label1.BackColor = Color.Transparent;
            label2.BackColor = Color.Transparent;
            label3.BackColor = Color.Transparent;
            label4.BackColor = Color.Transparent;
            label5.BackColor = Color.Transparent;

            
            
        }


        private void Q1FalseClicked3(object sender, EventArgs e)
        {

            
            Form1.q2.Visible = true;
            Form1.q1.Visible = false;
        }

        private void Q1FalseClicked2(object sender, EventArgs e)
        {

           
            Form1.q2.Visible = true;
            Form1.q1.Visible = false;
        }

        private void Q1False_Clicked1(object sender, EventArgs e)
        {
            
            Form1.q2.Visible = true;
            Form1.q1.Visible = false;
        }

        private void Q1TrueClicked(object sender, EventArgs e)
        {

            
            Form1.q2.Visible = true;
            Form1.q1.Visible = false;
            Form1.score++;
            
        }
    }
}
