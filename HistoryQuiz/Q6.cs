﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HistoryQuiz
{
    public partial class Q6 : UserControl
    {
        public Q6()
        {
            InitializeComponent();
            label1.BackColor = Color.Transparent;
            label2.BackColor = Color.Transparent;
            label3.BackColor = Color.Transparent;
            label4.BackColor = Color.Transparent;
            label5.BackColor = Color.Transparent;
        }

        private void Q6True(object sender, EventArgs e)
        {
            Form1.score++;
            Form1.q7.Visible = true;
            Form1.q6.Visible = false;
        }

        private void Q6False(object sender, EventArgs e)
        {
            
            Form1.q7.Visible = true;
            Form1.q6.Visible = false;
        }
    }
}
