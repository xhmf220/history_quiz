﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HistoryQuiz
{
    public partial class Result : UserControl
    {
        public Result()
        {
            InitializeComponent();
        }

        public void ShowResult()
        {
            if (Form1.score == 10)
            {
                himikoText.Font = new Font(himikoText.Font.FontFamily, 18);
                this.resultText.Text = "結果は10問中『全問正解』です！";
                this.himikoText.Text = "そなた、\r\nなかなか博識じゃな！\r\n賢い者、わらわは好きじゃぞ♡";
                himikoPicture.Image = System.Drawing.Image.FromFile(@"C:\Users\190416PM\Documents\C#\HistoryQuiz\HistoryQuiz\Resources\b613a5f73a067a13d7e7df27bdd73924.jpeg");
                himikoPicture.Size = new Size(275, 350);
                effectPicture.Image = System.Drawing.Image.FromFile(@"C:\Users\190416PM\Documents\C#\HistoryQuiz\HistoryQuiz\Resources\haato.jpg");
            }
            else if (Form1.score<=9 && Form1.score>=5 )
            {
                himikoText.Font = new Font(himikoText.Font.FontFamily, 19);
                this.resultText.Text = "結果は10問中『" + Form1.score + "問正解』です！";
                this.himikoText.Text = "ふむ…\r\nまずまずの結果じゃな。\r\nまぁ今後も精進するがよい。";
                himikoPicture.Image = System.Drawing.Image.FromFile(@"C:\Users\190416PM\Documents\C#\HistoryQuiz\HistoryQuiz\Resources\6lqeffaJ.jpeg");
            }
            else
            {
                this.resultText.Text = "結果は10問中『" + Form1.score + "問正解』です！";
                this.himikoText.Text = "全然駄目ではないか！\r\n貴様を邪馬台国から\r\n追放してくれようぞ！";
                himikoPicture.Image = System.Drawing.Image.FromFile(@"C:\Users\190416PM\Documents\C#\HistoryQuiz\HistoryQuiz\Resources\6lqeffaJ.jpeg");
                effectPicture.Image = System.Drawing.Image.FromFile(@"C:\Users\190416PM\Documents\C#\HistoryQuiz\HistoryQuiz\Resources\kekkon.jpg");
            }
        }
    }
}
