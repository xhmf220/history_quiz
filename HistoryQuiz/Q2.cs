﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HistoryQuiz
{
    public partial class Q2 : UserControl
    {
        public static Q2 ctr2;
        public Q2()
        {
            InitializeComponent();
            label1.BackColor = Color.Transparent;
            label2.BackColor = Color.Transparent;
            label3.BackColor = Color.Transparent;
            label4.BackColor = Color.Transparent;
            label5.BackColor = Color.Transparent;
        }

        private void Q2FalseClicked1(object sender, EventArgs e)
        {
            Form1.q3.Visible = true;
            Form1.q2.Visible = false;
        }

        private void Q2FalseClicked3(object sender, EventArgs e)
        {
            Form1.q3.Visible = true;
            Form1.q2.Visible = false;
        }

     

        private void Q2TrueClicked(object sender, EventArgs e)
        {
            Form1.score++;
            Form1.q3.Visible = true;
            Form1.q2.Visible = false;
        }

        private void Q2FalseClicked4(object sender, EventArgs e)
        {
            Form1.q3.Visible = true;
            Form1.q2.Visible = false;
        }
    }
}
