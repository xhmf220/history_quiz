﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HistoryQuiz
{
    public partial class Q9 : UserControl
    {
        public Q9()
        {
            InitializeComponent();
            label1.BackColor = Color.Transparent;
            label2.BackColor = Color.Transparent;
            label3.BackColor = Color.Transparent;
            label4.BackColor = Color.Transparent;
            label5.BackColor = Color.Transparent;
        }

        private void Q9F(object sender, EventArgs e)
        {
            
            Form1.q10.Visible = true;
            Form1.q9.Visible = false;
        }

        private void Q9T(object sender, EventArgs e)
        {
            Form1.score++;
            Form1.q10.Visible = true;
            Form1.q9.Visible = false;
        }
    }
}
