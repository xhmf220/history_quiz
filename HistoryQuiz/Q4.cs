﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HistoryQuiz
{
    public partial class Q4 : UserControl
    {
        public Q4()
        {
            InitializeComponent();
            label1.BackColor = Color.Transparent;
            label2.BackColor = Color.Transparent;
            label3.BackColor = Color.Transparent;
            label4.BackColor = Color.Transparent;
            label5.BackColor = Color.Transparent;
        }

        private void Q4TrueClicked(object sender, EventArgs e)
        {
            Form1.score++;
            Form1.q5.Visible = true;
            Form1.q4.Visible = false;
        }

        private void Q4FalseClicked1(object sender, EventArgs e)
        {
            Form1.q5.Visible = true;
            Form1.q4.Visible = false;
        }

        private void Q4FalseClicked2(object sender, EventArgs e)
        {
            Form1.q5.Visible = true;
            Form1.q4.Visible = false;
        }

        private void Q4FalseClicked3(object sender, EventArgs e)
        {
            Form1.q5.Visible = true;
            Form1.q4.Visible = false;
        }
       
    }
}
