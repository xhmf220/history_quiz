﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HistoryQuiz
{
    public partial class Q10 : UserControl
    {
        public Q10()
        {
            InitializeComponent();
            label1.BackColor = Color.Transparent;
            label2.BackColor = Color.Transparent;
            label3.BackColor = Color.Transparent;
            label4.BackColor = Color.Transparent;
            label5.BackColor = Color.Transparent;
        }

        private void Q10F(object sender, EventArgs e)
        {

            Form1.result.Visible = true;
            Form1.q10.Visible = false;
            Form1.result.ShowResult();
        }

        private void Q10T(object sender, EventArgs e)
        {
            Form1.score++;
            Form1.result.Visible = true;
            Form1.q10.Visible = false;
            Form1.result.ShowResult();
        }
    }
}
