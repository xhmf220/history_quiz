﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HistoryQuiz
{
    public partial class Q5 : UserControl
    {
        public Q5()
        {
            InitializeComponent();
            label1.BackColor = Color.Transparent;
            label2.BackColor = Color.Transparent;
            label3.BackColor = Color.Transparent;
            label4.BackColor = Color.Transparent;
            label5.BackColor = Color.Transparent;
        }

        private void Q5TrueClicked(object sender, EventArgs e)
        {
            Form1.score++;
            Form1.q6.Visible = true;
            Form1.q5.Visible = false;
            
        }

        private void Q5FalseClicked(object sender, EventArgs e)
        {
            Form1.q6.Visible = true;
            Form1.q5.Visible = false;
        }

        private void Q5FalseClicked2(object sender, EventArgs e)
        {
            Form1.q6.Visible = true;
            Form1.q5.Visible = false;
        }

        private void Q5FalseClicked3(object sender, EventArgs e)
        {
            Form1.q6.Visible = true;
            Form1.q5.Visible = false;


        }
    }
}
