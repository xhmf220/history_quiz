﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HistoryQuiz
{
    public partial class Q3 : UserControl
    {
        public Q3()
        {
            InitializeComponent();
            label1.BackColor = Color.Transparent;
            label2.BackColor = Color.Transparent;
            label3.BackColor = Color.Transparent;
            label4.BackColor = Color.Transparent;
            label5.BackColor = Color.Transparent;
        }

        private void Q3TrueClicked(object sender, EventArgs e)
        {
            Form1.score++;
            Form1.q4.Visible = true;
            Form1.q3.Visible = false;
        }

        private void Q3FalseClicked(object sender, EventArgs e)
        {
            Form1.q4.Visible = true;
            Form1.q3.Visible = false;
        }

        private void Q3FalseClicked2(object sender, EventArgs e)
        {
            Form1.q4.Visible = true;
            Form1.q3.Visible = false;
        }

        private void Q3FalseClicked3(object sender, EventArgs e)
        {
            Form1.q4.Visible = true;
            Form1.q3.Visible = false;
        }
    }
}
